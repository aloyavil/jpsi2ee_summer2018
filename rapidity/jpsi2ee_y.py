#lb-run Gaudi/latest python
#from AllMacroes import *
from ROOT import *
gROOT.ProcessLine(".x ~pkoppenb/public/lhcbStyle.C")

"""
-rw-r--r--. 1 pkoppenb z5    29930083 Jun 21 13:59 CEP-Jpsi2Mumu-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5237584 Jun 21 13:59 CEP-Chic1-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5164408 Jun 21 13:59 CEP-Chic0-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5388376 Jun 21 13:59 CEP-Chic2-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5   898318977 Jun 21 21:57 CEP-2016-Down-1603-1604-1605-Small.root
"""
_data = TFile.Open("/eos/lhcb/user/p/pkoppenb/Jpsi2ee/CEP-2016-Down-1603-1604-1605-Small.root")
_data.ls()
mm=_data.Get("Jpsi2ee") # dielectrons

#Observables
mass = RooRealVar("mass","Mass(e^{+}e^{-}) [MeV/c^{2}]",2000.,3500.)  #before psi(2s) peak  
y = RooRealVar("y", "pseudorapidity",0.,6.)
epBrem = RooRealVar("epBrem","e^{+} Bremsstrahlung #gamma",0.,'Inf')
emBrem = RooRealVar("emBrem","e^{-} Bremsstrahlung #gamma",0.,'Inf')

#data
dataset = RooDataSet("data","data",mm,RooArgSet(mass,y,epBrem,emBrem))  #mass, e+ & e- brem, pseudorapidity

""" Data Parse: y = 4.5-2 = 2.5/.25 = 10 x 3 bs = 33 fits
Start with brem = 2, cleanest signal"""

#----------- START BREM = 2 CATEGORY---------------

#data parse for brem = 2
dataset2brem_1 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 2.00 && y < 2.25")
dataset2brem_2 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 2.25 && y < 2.50")
dataset2brem_3 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 2.50 && y < 2.75")
dataset2brem_4 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 2.75 && y < 3.00")
dataset2brem_5 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 3.00 && y < 3.25")
dataset2brem_6 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 3.25 && y < 3.50")
dataset2brem_7 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 3.50 && y < 3.75")
dataset2brem_8 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 3.75 && y < 4.00")
dataset2brem_9 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 4.00 && y < 4.25")
dataset2brem_10 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2. && y >= 4.25 && y <= 4.50")


#plots on canv0 of data
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 2" ,1800,1800)
canv0.Divide(5,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("2.00 < y < 2.25")
dataset2brem_1.plotOn(plot1)
plot1.Draw()
canv0.cd(2)
plot2 = mass.frame()
plot2.SetTitle("2.25 < y < 2.50")
dataset2brem_2.plotOn(plot2)
plot2.Draw()
canv0.cd(3)
plot3 = mass.frame()
plot3.SetTitle("2.50 < y < 2.75")
dataset2brem_3.plotOn(plot3)
plot3.Draw()
canv0.cd(4)
plot4 = mass.frame()
plot4.SetTitle("2.75 < y < 3.00")
dataset2brem_4.plotOn(plot4)
plot4.Draw()
canv0.cd(5)
plot5 = mass.frame()
plot5.SetTitle("3.00 < y < 3.25")
dataset2brem_5.plotOn(plot5)
plot5.Draw()
canv0.cd(6)
plot6 = mass.frame()
plot6.SetTitle("3.25 < y < 3.50")
dataset2brem_6.plotOn(plot6)
plot6.Draw()
canv0.cd(7)
plot7 = mass.frame()
plot7.SetTitle("3.50 < y < 3.75")
dataset2brem_7.plotOn(plot7)
plot7.Draw()
canv0.cd(8)
plot8 = mass.frame()
plot8.SetTitle("3.75 < y < 4.00")
dataset2brem_8.plotOn(plot8)
plot8.Draw()
canv0.cd(9)
plot9 = mass.frame()
plot9.SetTitle("4.00 < y < 4.25")
dataset2brem_9.plotOn(plot9)
plot9.Draw()
canv0.cd(10)
plot10 = mass.frame()
plot10.SetTitle("4.25 < y < 4.50")
dataset2brem_10.plotOn(plot10)
plot10.Draw()


#fit parameters 
mean2 = RooRealVar("mean2","J/#Psi Peak",3050.,3000.,3200.)   
cb2_sigma = RooRealVar("cb2_sigma","sigma of crystal ball 0",30.,0.,70.)
cb2_n = RooRealVar("cb2_n","n of CB",10.,5.,15.)                             
cb2_a = RooRealVar("cb2_a","alpha of CB",1.5,0.,4.)                         
slope2 = RooRealVar("slope2","exponential slope",-0.002,-0.01,0.) 
c0 = RooRealVar("c0","coefficient #0", 1.0,-20.,20.) #for chebychev if needed
#contributions & fractions
CB2 = RooCBShape("CB2","Crystal Ball of First Peak",mass,mean2,cb2_sigma,cb2_a,cb2_n)
#chev2 = RooChebychev("chev2","background",mass,RooArgList(c0))
exp2 = RooExponential("exp2","background",mass,slope2) 
frac_cb2 = RooRealVar("frac_cb2","signal fraction CB",0.75,0.,1.0) 
#PDF with chebychev and exp (depends on how flat bg is)
pdf2 = RooAddPdf("pdf2","signal pdf",RooArgList(CB2,exp2),RooArgList(frac_cb2))

#fits + plots (eventually loop through 1-10! For now change fit & dataset number and y range)
fit2_2 = pdf2.fitTo(dataset2brem_2,RooFit.Save())

#create canvas
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 2" ,1800,1800)
canv0.Divide(2,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 2 & 2.25 < y < 2.50")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
dataset2brem_2.plotOn(plot1, RooFit.Name("Data2"))
pdf2.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit2")) 
pdf2.plotOn(plot1,RooFit.Components("CB2"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB2"))
#background contribution
pdf2.plotOn(plot1,RooFit.Components("exp2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp2"))
#pdf2.plotOn(plot1,RooFit.Components("chev2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev2"))
plot1.Draw()
gPad.SetLogy() 
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot1.findObject("Data2"),"LHCb (#sqrt{s} = 13 TeV, brem = 2)")
leg2.AddEntry(plot1.findObject("Fit2"),"Fit")
leg2.AddEntry(plot1.findObject("CB2"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg2.AddEntry(plot1.findObject("exp2"),"Exp")
#leg2.AddEntry(plot1.findObject("Chev2"),"Chebychev")
leg2.Draw()
canv0.cd(3)
pulls2 = plot1.pullHist("Data2","Fit2")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()
canv0.cd(2)
plot1 = mass.frame()
dataset2brem_2.plotOn(plot1,RooFit.Name("Data2"))
pdf2.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit2")) 
pdf2.plotOn(plot1,RooFit.Components("CB2"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB2")) 
#pdf2.plotOn(plot2,RooFit.Components("CB2_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB2_inv"))
#background contribution
pdf2.plotOn(plot1,RooFit.Components("exp2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp2"))
#pdf2.plotOn(plot1,RooFit.Components("chev2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev2"))
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 2 & 2.25 < y < 2.50")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot1.Draw()
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot1.findObject("Data2"),"LHCb (#sqrt{s} = 13 TeV, brem = 2)")
leg2.AddEntry(plot1.findObject("Fit2"),"Fit")
leg2.AddEntry(plot1.findObject("CB2"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg2.AddEntry(plot1.findObject("exp2"),"Exp")
#leg2.AddEntry(plot1.findObject("chev2"),"Chebychev")
leg2.Draw()
canv0.cd(4)
pulls2 = plot1.pullHist("Data2","Fit2")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()


#----------- END BREM = 2 CATEGORY---------------


#----------- START BREM = 1 CATEGORY---------------

#data parse for brem = 1
dataset1brem_1 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 2.00 && y < 2.25")
dataset1brem_2 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 2.25 && y < 2.50")
dataset1brem_3 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 2.50 && y < 2.75")
dataset1brem_4 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 2.75 && y < 3.00")
dataset1brem_5 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 3.00 && y < 3.25")
dataset1brem_6 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 3.25 && y < 3.50")
dataset1brem_7 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 3.50 && y < 3.75")
dataset1brem_8 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 3.75 && y < 4.00")
dataset1brem_9 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 4.00 && y < 4.25")
dataset1brem_10 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1. && y >= 4.25 && y <= 4.50")

#plots on canv0 of data
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 1" ,1800,1800)
canv0.Divide(5,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("2.00 < y < 2.25")
dataset1brem_1.plotOn(plot1)
plot1.Draw()
canv0.cd(2)
plot2 = mass.frame()
plot2.SetTitle("2.25 < y < 2.50")
dataset1brem_2.plotOn(plot2)
plot2.Draw()
canv0.cd(3)
plot3 = mass.frame()
plot3.SetTitle("2.50 < y < 2.75")
dataset1brem_3.plotOn(plot3)
plot3.Draw()
canv0.cd(4)
plot4 = mass.frame()
plot4.SetTitle("2.75 < y < 3.00")
dataset1brem_4.plotOn(plot4)
plot4.Draw()
canv0.cd(5)
plot5 = mass.frame()
plot5.SetTitle("3.00 < y < 3.25")
dataset1brem_5.plotOn(plot5)
plot5.Draw()
canv0.cd(6)
plot6 = mass.frame()
plot6.SetTitle("3.25 < y < 3.50")
dataset1brem_6.plotOn(plot6)
plot6.Draw()
canv0.cd(7)
plot7 = mass.frame()
plot7.SetTitle("3.50 < y < 3.75")
dataset1brem_7.plotOn(plot7)
plot7.Draw()
canv0.cd(8)
plot8 = mass.frame()
plot8.SetTitle("3.75 < y < 4.00")
dataset1brem_8.plotOn(plot8)
plot8.Draw()
canv0.cd(9)
plot9 = mass.frame()
plot9.SetTitle("4.00 < y < 4.25")
dataset1brem_9.plotOn(plot9)
plot9.Draw()
canv0.cd(10)
plot10 = mass.frame()
plot10.SetTitle("4.25 < y < 4.50")
dataset1brem_10.plotOn(plot10)
plot10.Draw()

#fit parameters 
mean1 = RooRealVar("mean1","J/#Psi Peak",3050.,3000.,3200.)   
cb1_sigma = RooRealVar("cb1_sigma","sigma of crystal ball 0",30.,0.,70.)
cb1_n = RooRealVar("cb1_n","n of CB",10.,5.,15.)                             
cb1_a = RooRealVar("cb1_a","alpha of CB",1.5,0.,4.)       
cb1_sigma_inv = RooRealVar("cb1_sigma_inv","sigma of inverted crystal ball 0",30.,0.,70.)
cb1_n_inv = RooRealVar("cb1_n_inv","n of inverted CB",10.,5.,15.)                             
cb1_a_inv = RooRealVar("cb1_a_inv","alpha of inverted CB",-1.5,-3.,0.)  
slope1 = RooRealVar("slope1","exponential slope",-0.002,-0.01,0.) 
c0 = RooRealVar("c0","coefficient #0", 1.0,-20.,20.) #for chebychev if needed
#contributions & fractions
CB1 = RooCBShape("CB1","Crystal Ball of First Peak",mass,mean1,cb1_sigma,cb1_a,cb1_n)
CB1_inv = RooCBShape("CB1_inv","Inverted Crystal Ball of First Peak",mass,mean1,cb1_sigma_inv,cb1_a_inv,cb1_n_inv)
#chev2 = RooChebychev("chev1","background",mass,RooArgList(c0))
exp1 = RooExponential("exp1","background",mass,slope1) 
frac_cb1 = RooRealVar("frac_cb1","signal fraction CB",0.4,0.,1.0) 
frac_cb1_inv = RooRealVar("frac_cb1_inv","signal fraction inverted CB",0.4,0.,1.0) 
#PDF with chebychev and exp (depends on how flat bg is)
pdf1 = RooAddPdf("pdf1","signal pdf",RooArgList(CB1,exp1),RooArgList(frac_cb1))

#fits + plots (eventually loop through 1-10! For now change fit & dataset number and y range)
fit1_10 = pdf1.fitTo(dataset1brem_10,RooFit.Save())

#create canvas
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 1" ,1800,1800)
canv0.Divide(2,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 1 & 4.25 < y < 4.50")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
dataset1brem_10.plotOn(plot1, RooFit.Name("Data1"))
pdf1.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit1")) 
pdf1.plotOn(plot1,RooFit.Components("CB1"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB1"))
#pdf1.plotOn(plot1,RooFit.Components("CB1_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB1_inv"))
#background contribution
pdf1.plotOn(plot1,RooFit.Components("exp1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp1"))
#pdf1.plotOn(plot1,RooFit.Components("chev1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev1"))
plot1.Draw()
gPad.SetLogy() 
leg1 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg1.SetHeader("Legend","C") #C centers the title
leg1.AddEntry(plot1.findObject("Data1"),"LHCb (#sqrt{s} = 13 TeV, brem = 1)")
leg1.AddEntry(plot1.findObject("Fit1"),"Fit")
leg1.AddEntry(plot1.findObject("CB1"),"CB")
#leg1.AddEntry(plot1.findObject("CB1_inv"),"CB Inverted")
leg1.AddEntry(plot1.findObject("exp1"),"Exp")
#leg2.AddEntry(plot1.findObject("Chev2"),"Chebychev")
leg1.Draw()
canv0.cd(3)
pulls2 = plot1.pullHist("Data1","Fit1")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()
canv0.cd(2)
plot1 = mass.frame()
dataset1brem_10.plotOn(plot1,RooFit.Name("Data1"))
pdf1.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit1")) 
pdf1.plotOn(plot1,RooFit.Components("CB1"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB1")) 
#pdf1.plotOn(plot1,RooFit.Components("CB1_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB1_inv"))
#background contribution
pdf1.plotOn(plot1,RooFit.Components("exp1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp1"))
#pdf1.plotOn(plot1,RooFit.Components("chev1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev1"))
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 1 & 4.25 < y < 4.50")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot1.Draw()
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot1.findObject("Data1"),"LHCb (#sqrt{s} = 13 TeV, brem = 1)")
leg2.AddEntry(plot1.findObject("Fit1"),"Fit")
leg2.AddEntry(plot1.findObject("CB1"),"CB")
#leg2.AddEntry(plot1.findObject("CB1_inv"),"CB Inverted")
leg2.AddEntry(plot1.findObject("exp1"),"Exp")
#leg2.AddEntry(plot1.findObject("chev2"),"Chebychev")
leg2.Draw()
canv0.cd(4)
pulls2 = plot1.pullHist("Data1","Fit1")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()


#----------- END BREM = 1 CATEGORY---------------

#----------- START BREM = 0 CATEGORY---------------

#data parse for brem = 1
dataset0brem_1 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 2.00 && y < 2.25")
dataset0brem_2 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 2.25 && y < 2.50")
dataset0brem_3 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 2.50 && y < 2.75")
dataset0brem_4 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 2.75 && y < 3.00")
dataset0brem_5 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 3.00 && y < 3.25")
dataset0brem_6 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 3.25 && y < 3.50")
dataset0brem_7 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 3.50 && y < 3.75")
dataset0brem_8 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 3.75 && y < 4.00")
dataset0brem_9 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 4.00 && y < 4.25")
dataset0brem_10 = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0. && y >= 4.25 && y <= 4.50")

#plots on canv0 of data
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 0" ,1800,1800)
canv0.Divide(5,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("2.00 < y < 2.25")
dataset0brem_1.plotOn(plot1)
plot1.Draw()
canv0.cd(2)
plot2 = mass.frame()
plot2.SetTitle("2.25 < y < 2.50")
dataset0brem_2.plotOn(plot2)
plot2.Draw()
canv0.cd(3)
plot3 = mass.frame()
plot3.SetTitle("2.50 < y < 2.75")
dataset0brem_3.plotOn(plot3)
plot3.Draw()
canv0.cd(4)
plot4 = mass.frame()
plot4.SetTitle("2.75 < y < 3.00")
dataset0brem_4.plotOn(plot4)
plot4.Draw()
canv0.cd(5)
plot5 = mass.frame()
plot5.SetTitle("3.00 < y < 3.25")
dataset0brem_5.plotOn(plot5)
plot5.Draw()
canv0.cd(6)
plot6 = mass.frame()
plot6.SetTitle("3.25 < y < 3.50")
dataset0brem_6.plotOn(plot6)
plot6.Draw()
canv0.cd(7)
plot7 = mass.frame()
plot7.SetTitle("3.50 < y < 3.75")
dataset0brem_7.plotOn(plot7)
plot7.Draw()
canv0.cd(8)
plot8 = mass.frame()
plot8.SetTitle("3.75 < y < 4.00")
dataset0brem_8.plotOn(plot8)
plot8.Draw()
canv0.cd(9)
plot9 = mass.frame()
plot9.SetTitle("4.00 < y < 4.25")
dataset0brem_9.plotOn(plot9)
plot9.Draw()
canv0.cd(10)
plot10 = mass.frame()
plot10.SetTitle("4.25 < y < 4.50")
dataset0brem_10.plotOn(plot10)
plot10.Draw()

#fit parameters 
mean0 = RooRealVar("mean0","J/#Psi Peak",3050.,3000.,3200.)   
cb0_sigma = RooRealVar("cb0_sigma","sigma of crystal ball 0",30.,0.,70.)
cb0_n = RooRealVar("cb0_n","n of CB",10.,5.,15.)                             
cb0_a = RooRealVar("cb0_a","alpha of CB",1.5,0.,4.)       
cb0_sigma_inv = RooRealVar("cb1_sigma_inv","sigma of inverted crystal ball 0",30.,0.,70.)
cb0_n_inv = RooRealVar("cb0_n_inv","n of inverted CB",10.,5.,15.)                             
cb0_a_inv = RooRealVar("cb0_a_inv","alpha of inverted CB",-1.5,-3.,0.)  
slope0 = RooRealVar("slope0","exponential slope",-0.002,-0.01,0.) 
c0 = RooRealVar("c0","coefficient #0", 1.0,-20.,20.) #for chebychev if needed
#contributions & fractions
CB0 = RooCBShape("CB0","Crystal Ball of First Peak",mass,mean0,cb0_sigma,cb0_a,cb0_n)
CB0_inv = RooCBShape("CB0_inv","Inverted Crystal Ball of First Peak",mass,mean0,cb0_sigma_inv,cb0_a_inv,cb0_n_inv)
#chev2 = RooChebychev("chev1","background",mass,RooArgList(c0))
exp0 = RooExponential("exp0","background",mass,slope0) 
frac_cb0 = RooRealVar("frac_cb0","signal fraction CB",0.4,0.,1.0) 
frac_cb0_inv = RooRealVar("frac_cb0_inv","signal fraction inverted CB",0.4,0.,1.0) 
#PDF with chebychev and exp (depends on how flat bg is)
pdf0 = RooAddPdf("pdf0","signal pdf",RooArgList(CB0,exp0),RooArgList(frac_cb0))

#fits + plots (eventually loop through 1-10! For now change fit & dataset number and y range)
fit0_8 = pdf0.fitTo(dataset0brem_8,RooFit.Save())

#create canvas
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 0" ,1800,1800)
canv0.Divide(2,2)
canv0.cd(1)
plot1 = mass.frame()
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 0 & 3.75 < y < 4.00")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
dataset0brem_8.plotOn(plot1, RooFit.Name("Data0"))
pdf0.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit0")) 
pdf0.plotOn(plot1,RooFit.Components("CB0"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB0"))
#pdf0.plotOn(plot1,RooFit.Components("CB0_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB0_inv"))
#background contribution
pdf0.plotOn(plot1,RooFit.Components("exp0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp0"))
#pdf1.plotOn(plot1,RooFit.Components("chev0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev0"))
plot1.Draw()
gPad.SetLogy() 
leg1 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg1.SetHeader("Legend","C") #C centers the title
leg1.AddEntry(plot1.findObject("Data0"),"LHCb (#sqrt{s} = 13 TeV, brem = 0)")
leg1.AddEntry(plot1.findObject("Fit0"),"Fit")
leg1.AddEntry(plot1.findObject("CB0"),"CB")
#leg1.AddEntry(plot1.findObject("CB0_inv"),"CB Inverted")
leg1.AddEntry(plot1.findObject("exp0"),"Exp")
#leg2.AddEntry(plot1.findObject("Chev2"),"Chebychev")
leg1.Draw()
canv0.cd(3)
pulls2 = plot1.pullHist("Data0","Fit0")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()
canv0.cd(2)
plot1 = mass.frame()
dataset0brem_8.plotOn(plot1,RooFit.Name("Data0"))
pdf0.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit0")) 
pdf0.plotOn(plot1,RooFit.Components("CB0"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB0")) 
pdf0.plotOn(plot1,RooFit.Components("CB0_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB0_inv"))
#background contribution
pdf0.plotOn(plot1,RooFit.Components("exp0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp0"))
#pdf0.plotOn(plot1,RooFit.Components("chev0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("chev0"))
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 0 & 3.75 < y < 4.00")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot1.Draw()
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot1.findObject("Data0"),"LHCb (#sqrt{s} = 13 TeV, brem = 0)")
leg2.AddEntry(plot1.findObject("Fit0"),"Fit")
leg2.AddEntry(plot1.findObject("CB0"),"CB")
#leg2.AddEntry(plot1.findObject("CB0_inv"),"CB Inverted")
leg2.AddEntry(plot1.findObject("exp0"),"Exp")
#leg2.AddEntry(plot1.findObject("chev0"),"Chebychev")
leg2.Draw()
canv0.cd(4)
pulls2 = plot1.pullHist("Data0","Fit0")
pulls2.SetTitle("Pulls of Histogram")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()


#----------- END BREM = 0 CATEGORY---------------



