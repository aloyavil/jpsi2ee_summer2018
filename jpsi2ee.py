#lb-run Gaudi/latest python
#from AllMacroes import *
from ROOT import *
gROOT.ProcessLine(".x ~pkoppenb/public/lhcbStyle.C")

"""
-rw-r--r--. 1 pkoppenb z5    29930083 Jun 21 13:59 CEP-Jpsi2Mumu-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5237584 Jun 21 13:59 CEP-Chic1-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5164408 Jun 21 13:59 CEP-Chic0-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5     5388376 Jun 21 13:59 CEP-Chic2-2015-Sim09b.root
-rw-r--r--. 1 pkoppenb z5   898318977 Jun 21 21:57 CEP-2016-Down-1603-1604-1605-Small.root
"""
_data = TFile.Open("/eos/lhcb/user/p/pkoppenb/Jpsi2ee/CEP-2016-Down-1603-1604-1605-Small.root")
_data.ls()
mm=_data.Get("Jpsi2ee") # dielectrons

#Observables
mass = RooRealVar("mass","Mass(e^{+}e^{-}) [MeV/c^{2}]",2000.,3500.)  #before psi(2s) peak  
y = RooRealVar("y", "pseudorapidity",2.,5.)
epBrem = RooRealVar("epBrem","e^{+} Bremsstrahlung #gamma",0.,'Inf')
emBrem = RooRealVar("emBrem","e^{-} Bremsstrahlung #gamma",0.,'Inf')

#data parse
dataset = RooDataSet("data","data",mm,RooArgSet(mass,y,epBrem,emBrem))  #mass, e+ & e- brem, pseudorapidity  
dataset0brem = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 0")     #0 photons added; use && for next condition y
dataset1brem = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 1")     #1 photons added
dataset2brem = dataset.reduce("((epBrem > 0) + (emBrem > 0)) == 2")     #2 photons added

"""
#plot on canvas
canv1 = TCanvas("canv1","CEP of J/#psi -> e^{+} e^{-} Original",800,800)
canv1.Divide(1,3)
canv1.cd(1)
plot0 = mass.frame()
dataset0brem.plotOn(plot0)
plot0.Draw()
canv1.cd(2)
plot1 = mass.frame()
dataset1brem.plotOn(plot1)
plot1.Draw()
canv1.cd(3)
plot2 = mass.frame()
dataset2brem.plotOn(plot2)
plot2.Draw()
"""

#parameters 0
mean0 = RooRealVar("mean0","J/Psi Peak",3050.,3000.,3200.)  
cb0_sigma = RooRealVar("cb0_sigma","sigma of crystal ball 0",30.,0.,100.)
cb0_sigma_inv = RooRealVar("cb0_sigma_inv","sigma of inverted crystal ball 0",60.,0.,200.)
cb0_n = RooRealVar("cb0_n","n of CB",10.,0.,15.)                             # radiative tail parameter n
cb0_a = RooRealVar("cb0_a","alpha of CB",1.5,0.,3.)                         # radiative tail parameter a
slope0 = RooRealVar("slope0","exponential slope",-0.002,-0.01,0.)            # negative slope of exponential
cb0_n_inv = RooRealVar("cb0_n_inv","n of CB inverse",5.,0.,15.)                               # inverse CB
cb0_a_inv = RooRealVar("cb0_a_inv","alpha of CB inverse",-1.5,-3.,0.)  
#contributions, fractions and PDF 
CB0 = RooCBShape("CB0","Crystal Ball of First Peak",mass,mean0,cb0_sigma,cb0_a,cb0_n) 
exp0 = RooExponential("exp0","Exponential Background",mass,slope0) 
CB0_inv = RooCBShape("CB0_inv","Inverted Crystal Ball of First Peak", mass,mean0,cb0_sigma_inv,cb0_a_inv,cb0_n_inv) 
frac_cb0 = RooRealVar("frac_cb0","signal fraction CB",0.2,0.05,0.5) 
frac_cb0_inv = RooRealVar("frac_cb0_inv","signal fraction CB",0.05,0.,0.5) 
pdf0 = RooAddPdf("pdf0","signal pdf",RooArgList(CB0,exp0),RooArgList(frac_cb0))


#parameters 1
mean1 = RooRealVar("mean1","J/#Psi Peak",3050.,3000.,3200.)   
cb1_sigma = RooRealVar("cb1_sigma","sigma of crystal ball 0",50.,0.,100.)
cb1_sigma_inv = RooRealVar("cb1_sigma_inv","sigma of inverted crystal ball 0",100.,0.,200.)
cb1_n = RooRealVar("cb1_n","n of CB",10.,0.,15.)                             # radiative tail parameter n
cb1_a = RooRealVar("cb1_a","alpha of CB",1.5,0.,3.)                         # radiative tail parameter a
slope1 = RooRealVar("slope1","exponential slope",-0.002,-0.01,0.)            # negative slope of exponential
cb1_n_inv = RooRealVar("cb1_n_inv","n of CB inverse",5.,0.,15.)                               # inverse CB
cb1_a_inv = RooRealVar("cb1_a_inv","alpha of CB inverse",-2.0,-5.,0.)
#contributions, fractions and PDF 
CB1 = RooCBShape("CB1","Crystal Ball of First Peak",mass,mean1,cb1_sigma,cb1_a,cb1_n) 
exp1 = RooExponential("exp1","Exponential Background",mass,slope1) 
CB1_inv = RooCBShape("CB1_inv","Inverted Crystal Ball of First Peak", mass,mean1,cb1_sigma_inv,cb1_a_inv,cb1_n_inv) 
frac_cb1 = RooRealVar("frac_cb1","signal fraction CB",0.1,0.,0.5) 
frac_cb1_inv = RooRealVar("frac_cb1_inv","signal fraction CB",0.05,0.,0.5) 
pdf1 = RooAddPdf("pdf1","signal pdf",RooArgList(CB1,exp1),RooArgList(frac_cb1))

#parameters 2  
mean2 = RooRealVar("mean2","J/#Psi Peak",3050.,3000.,3200.)   
cb2_sigma = RooRealVar("cb2_sigma","sigma of crystal ball 0",30.,0.,70.)
cb2_n = RooRealVar("cb2_n","n of CB",10.,5.,15.)                             
cb2_a = RooRealVar("cb2_a","alpha of CB",1.5,0.,4.)                         
slope2 = RooRealVar("slope2","exponential slope",-0.002,-0.01,0.)            
#contributions, fractions and PDF 
CB2 = RooCBShape("CB2","Crystal Ball of First Peak",mass,mean2,cb2_sigma,cb2_a,cb2_n) 
exp2 = RooExponential("exp2","Exponential Background",mass,slope2) 
frac_cb2 = RooRealVar("frac_cb2","signal fraction CB",0.4,0.,1.0) 
pdf2 = RooAddPdf("pdf2","signal pdf",RooArgList(CB2,exp2),RooArgList(frac_cb2))


#fits
fit0 = pdf0.fitTo(dataset0brem,RooFit.Save())
fit1 = pdf1.fitTo(dataset1brem,RooFit.Save())
fit2 = pdf2.fitTo(dataset2brem,RooFit.Save())


#Plots (with or without fit) for each brem separately
#Brem = 0
canv0 = TCanvas("canv0","CEP of J/#psi -> e^{+} e^{-} for #gamma = 0 ",1800,1800)
canv0.Divide(2,2)
canv0.cd(1)
plot0 = mass.frame(RooFit.Title("J/ #Psi -> e^{+} e^{-} for #gamma = 0"))
dataset0brem.plotOn(plot0,RooFit.Name("Data0"))
pdf0.plotOn(plot0,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit0")) 
pdf0.plotOn(plot0,RooFit.Components("CB0"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB0")) 
#pdf0.plotOn(plot0,RooFit.Components("CB0_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB0_inv"))
pdf0.plotOn(plot0,RooFit.Components("exp0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp0")) 
plot0.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 0")
plot0.GetYaxis.SetTitle("Events / 15 MeV/c^{2}") #not workin
plot0.Draw()
gPad.SetLogy() 
leg0 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg0.SetHeader("Legend","C") #C centers the title
leg0.AddEntry(plot0.findObject("Data0"),"LHCb (#sqrt{s} = 13 TeV, brem = 0)")
leg0.AddEntry(plot0.findObject("Fit0"),"Fit")
leg0.AddEntry(plot0.findObject("CB0"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg0.AddEntry(plot0.findObject("exp0"),"Exp")
leg0.Draw()
canv0.cd(3)
pulls0 = plot0.pullHist("Data0","Fit0")
pulls0.GetYaxis().SetTitle("Pulls")
pulls0.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls0.Draw()
canv0.cd(2)
plot0 = mass.frame()
dataset0brem.plotOn(plot0,RooFit.Name("Data0"))
pdf0.plotOn(plot0,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit0")) 
pdf0.plotOn(plot0,RooFit.Components("CB0"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB0")) 
#pdf0.plotOn(plot0,RooFit.Components("CB0_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB0_inv"))
pdf0.plotOn(plot0,RooFit.Components("exp0"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp0")) 
plot0.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 0")
plot0.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot0.Draw() 
leg0 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg0.SetHeader("Legend","C") #C centers the title
leg0.AddEntry(plot0.findObject("Data0"),"LHCb (#sqrt{s} = 13 TeV, brem = 0)")
leg0.AddEntry(plot0.findObject("Fit0"),"Fit")
leg0.AddEntry(plot0.findObject("CB0"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg0.AddEntry(plot0.findObject("exp0"),"Exp")
leg0.Draw()
canv0.cd(4)
pulls0 = plot0.pullHist("Data0","Fit0")
pulls0.GetYaxis().SetTitle("Pulls")
pulls0.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls0.Draw()
canv0.SaveAs("brem01cb.root")

#Brem = 1
canv1 = TCanvas("canv1","CEP of J/#psi -> e^{+} e^{-} for #gamma = 1 ",1800,1800)
canv1.Divide(2,2)
canv1.cd(1)
plot1 = mass.frame(RooFit.Title("J/ #Psi -> e^{+} e^{-} for #gamma = 1"))
dataset1brem.plotOn(plot1,RooFit.Name("Data1"))
pdf1.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit1")) 
pdf1.plotOn(plot1,RooFit.Components("CB1"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB1")) 
#pdf1.plotOn(plot1,RooFit.Components("CB1_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB1_inv"))
pdf1.plotOn(plot1,RooFit.Components("exp1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp1")) 
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 1")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}") #not workin
plot1.Draw()
gPad.SetLogy() 
leg1 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg1.SetHeader("Legend","C") #C centers the title
leg1.AddEntry(plot1.findObject("Data1"),"LHCb (#sqrt{s} = 13 TeV, brem = 1)")
leg1.AddEntry(plot1.findObject("Fit1"),"Fit")
leg1.AddEntry(plot1.findObject("CB1"),"CB")
#leg1.AddEntry(plot1.findObject("CB1_inv"),"CB Inverted")
leg1.AddEntry(plot1.findObject("exp1"),"Exp")
leg1.Draw()
canv1.cd(3)
pulls1 = plot1.pullHist("Data1","Fit1")
pulls1.GetYaxis().SetTitle("Pulls")
pulls1.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls1.Draw()
canv1.cd(2)
plot1 = mass.frame()
dataset1brem.plotOn(plot1,RooFit.Name("Data1"))
pdf1.plotOn(plot1,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit1")) 
pdf1.plotOn(plot1,RooFit.Components("CB1"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB1")) 
#pdf1.plotOn(plot1,RooFit.Components("CB1_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB1_inv"))
pdf1.plotOn(plot1,RooFit.Components("exp1"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp1")) 
plot1.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 1")
plot1.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot1.Draw() 
leg1 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg1.SetHeader("Legend","C") #C centers the title
leg1.AddEntry(plot1.findObject("Data1"),"LHCb (#sqrt{s} = 13 TeV, brem = 1)")
leg1.AddEntry(plot1.findObject("Fit1"),"Fit")
leg1.AddEntry(plot1.findObject("CB1"),"CB")
#leg1.AddEntry(plot1.findObject("CB1_inv"),"CB Inverted")
leg1.AddEntry(plot1.findObject("exp1"),"Exp")
leg1.Draw()
canv1.cd(4)
pulls1 = plot1.pullHist("Data1","Fit1")
pulls1.GetYaxis().SetTitle("Pulls")
pulls1.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls1.Draw()


#Brem = 2
canv2 = TCanvas("canv2","CEP of J/#psi -> e^{+} e^{-} for #gamma = 2",1800,1800)
canv2.Divide(2,2)
canv2.cd(1)
plot2 = mass.frame()
dataset2brem.plotOn(plot2,RooFit.Name("Data2"))
pdf2.plotOn(plot2,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit2")) 
pdf2.plotOn(plot2,RooFit.Components("CB2"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB2")) 
#pdf2.plotOn(plot2,RooFit.Components("CB2_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB2_inv"))
pdf2.plotOn(plot2,RooFit.Components("exp2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp2")) 
plot2.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 2")
plot2.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot2.Draw()
gPad.SetLogy() 
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot2.findObject("Data2"),"LHCb (#sqrt{s} = 13 TeV, brem = 2)")
leg2.AddEntry(plot2.findObject("Fit2"),"Fit")
leg2.AddEntry(plot2.findObject("CB2"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg2.AddEntry(plot2.findObject("exp2"),"Exp")
leg2.Draw()
canv2.cd(3)
pulls2 = plot2.pullHist("Data2","Fit2")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()
canv2.cd(2)
plot2 = mass.frame()
dataset2brem.plotOn(plot2,RooFit.Name("Data2"))
pdf2.plotOn(plot2,RooFit.LineColor(kBlue),RooFit.LineStyle(kDashed),RooFit.Name("Fit2")) 
pdf2.plotOn(plot2,RooFit.Components("CB2"),RooFit.LineColor(kGreen),RooFit.LineStyle(kDashed),RooFit.Name("CB2")) 
#pdf2.plotOn(plot2,RooFit.Components("CB2_inv"),RooFit.LineColor(kYellow),RooFit.LineStyle(kDashed),RooFit.Name("CB2_inv"))
pdf2.plotOn(plot2,RooFit.Components("exp2"),RooFit.LineColor(kRed),RooFit.LineStyle(kDashed),RooFit.Name("exp2")) 
plot2.SetTitle("J/ #Psi -> e^{+} e^{-} for #gamma = 2")
plot0.GetYaxis.SetTitle("Events / 15 MeV/c^{2}")
plot2.Draw()
leg2 = TLegend(0.9,0.7,0.7,0.9) #3 is the width of table (larger val = narrow), 4
leg2.SetHeader("Legend","C") #C centers the title
leg2.AddEntry(plot2.findObject("Data2"),"LHCb (#sqrt{s} = 13 TeV, brem = 2)")
leg2.AddEntry(plot2.findObject("Fit2"),"Fit")
leg2.AddEntry(plot2.findObject("CB2"),"CB")
#leg0.AddEntry(plot0.findObject("CB0_inv"),"CB Inverted")
leg2.AddEntry(plot2.findObject("exp2"),"Exp")
leg2.Draw()
canv2.cd(4)
pulls2 = plot2.pullHist("Data2","Fit2")
pulls2.GetYaxis().SetTitle("Pulls")
pulls2.GetXaxis().SetTitle("Mass(e^{+} e^{-}) [MeV/c^{2}]")
pulls2.Draw()
canv2.SaveAs("brem2.root")

fit0.Print()
fit1.Print()
fit1.Print() 



